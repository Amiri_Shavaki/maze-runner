#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm> //std::find
#include <fstream>
#include <ctime>

using namespace std;

class Player {
    friend class Maze;

    private:
    int row;
    int col;

    public:
    int getRow() { return row; }
    int getCol() { return col; }
    void move(char dir);
    void moveBack(char dir);
};

void Player::moveBack(char dir) {
    const int yDir[] = {1, 0, -1, 0};
    const int xDir[] = {0, 2, 0, -2};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    for (int i = 0; i < 4; i++) {
        if (cDir[i] == dir) {
            row += yDir[i];
            col += xDir[i];
        }
    }
}

void Player::move(char dir) {
    const int yDir[] = {-1, 0, 1, 0};
    const int xDir[] = {0, -2, 0, 2};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    for (int i = 0; i < 4; i++) {
        if (cDir[i] == dir) {
            row += yDir[i];
            col += xDir[i];
        }
    }
}

#endif
