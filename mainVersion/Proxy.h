class Maze;

class Proxy {
    public:
    Proxy();
    void inputFromFiles();
    void setPlayerInitPos();
    void mainLoop();
    ~Proxy();

    private:
    Maze* ptr;
};
