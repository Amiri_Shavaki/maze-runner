#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm> //std::find
#include <fstream>
#include <ctime>
#include "Door.h"
#include "Player.h"

using namespace std;

class Maze {
    private:
    vector <string> map;
    vector <Door> Doors;
    const int DEFAULT_DEC;
    const int DEFAULT_T;
    Player player;
    const int SEPARATORLINELENGTH;
    const int SHARPLINELENGTH;
    const time_t t0;
    int remTime;

    public:
    void inputFromFiles();
    Maze(): DEFAULT_DEC(4321), DEFAULT_T(12345), SEPARATORLINELENGTH(10), SHARPLINELENGTH(10), t0(time(nullptr)), remTime(1500) {}
    void mainLoop();
    void setPlayerInitPos();
    void printMap();
    vector <char> printOptions();
    bool doorProcess();
    void clearMap();
    bool checkForWin();
    bool findS(string s);
    bool isSeparatorLine(string line);
    bool isSharpLine(string line);
    int convertStrToInt(string s);
    string convertIntToStr(int n);
};

int Maze::convertStrToInt(string s) {
    int ans = 0;
    for (int i = 0; i < s.size(); i++) {
        ans += (s[i] - '0');
        ans *= 10;
    }
    return ans;
}

string Maze::convertIntToStr(int n) {
    string res;
    while (n) {
        res += '0' + n % 10;
        n /= 10;
    }
    reverse(res.begin(), res.end());
    return res;
}

bool Maze::isSeparatorLine(string line) {
    if (line.size() != SEPARATORLINELENGTH) {
        return false;
    }
    for (int i = 0; i < line.size(); i++) {
        if (line[i] != '!') {
            return false;
        }
    }
    return true;
}

bool Maze::findS(string s) {
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == 'S') {
            return true;
        }
    }
    return false;
}

bool Maze::checkForWin() {
    if (player.row > 1) {
        return false;
    }
    for (int i = 0; i < map[0].size(); i++) {
        if (map[0][i] == 'O' && player.col - 1 == i) {
            return true;
        }
    }
    return false;
}

void Maze::clearMap() {
    char playerPosValue = map[player.row - 1][player.col - 1];
    if (playerPosValue == '#' || playerPosValue == 'T') {
        map[player.row - 1][player.col - 1] = ' ';
    }
}

bool Maze::doorProcess() {
    bool ans = true;
    for (int i = 0; i < Doors.size(); i++) {
        if (Doors[i].getRow() == player.row && Doors[i].getCol() == player.col && map[player.row - 1][player.col - 1] != ' ') {
            if (Doors[i].getIsBad()) { //BADZAT
                Doors[i].printProblem();
                cout << "Enter your answer: ";
                string givenSolution;
                cin.ignore();
                getline(cin, givenSolution);
                ans = Doors[i].checkSolution(givenSolution);
                if (!ans) { //Wrong answer
                    remTime -= Doors[i].getT() / 1000;
                    cout << "Decreased time: " << Doors[i].getT() / 1000 << " seconds\n";
                    cout << "Remaining time: " << remTime << " seconds\n";
                }
            } else { //KHOSHZAT
                remTime += Doors[i].getT() / 1000;
                cout << "Increased time: " << Doors[i].getT() / 1000 << " seconds\n";
                cout << "Remaining time: " << remTime << " seconds\n";
            }
        }
    }
    return ans;
}

vector <char> Maze::printOptions() {
    vector <char> ans;

    int y = player.row - 1; //0-based
    int x = player.col - 1; //0-based
    int n = map.size(); //number of rows
    int m = map[0].size(); //number of columns

    //correct order: U L D R
    const int yDir[] = {-1, 0, 0, 0};
    const int xDir[] = {0, -1, 0, 1};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    const string destDir[] = {"up", "left", "down", "right"};
    bool firstTime = true; //just a simple flag
    for (int i = 0; i < 4; i++) {
        int newY = y + yDir[i];
        int newX = x + xDir[i];
        if (newY >= n || newY < 0 || newX >= m || newX < 0) { //Can't go in the direction
            continue;
        }
        if (map[newY][newX] == '|' || map[newY][newX] == '_' || map[newY][newX] == 'S') { //Can't go in the direction
            continue;
        }
        if (firstTime) {
            firstTime = false;
        } else {
            cout << ", ";
        }
        cout << "Enter " << cDir[i] << " to go " << destDir[i];

        ans.push_back(cDir[i]);
    }
    cout << endl;
    return ans;
}

void Maze::printMap() {
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map[i].size(); j++) {
            if (player.row == i + 1 && player.col == j + 1) { //1-based conversion
                cout << 'P';
            } else if (map[i][j] == 'S' || map[i][j] == 'O') {
                cout << ' ';
            } else {
                cout << map[i][j];
            }
        }
        cout << endl;
    }
}

void Maze::setPlayerInitPos() {
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map[i].size(); j++) {
            if (map[i][j] == 'S') {
                player.row = i + 1; //1-based
                player.col = j + 1; //1-based
            }
        }
    }
}

void Maze::mainLoop() {
    bool gameOver = false;
    string choice;
    bool win;
    while (!gameOver) {
        printMap();
        cout << "Remaining time: " << remTime - time(nullptr) + t0 << " seconds\n";
        vector <char> options = printOptions();
        cin >> choice;
        if (find(options.begin(), options.end(), choice[0]) != options.end()) {
            player.move(choice[0]);
            if (!doorProcess()) {
                player.moveBack(choice[0]);
            } else {
                clearMap();
            }
            if (remTime - time(nullptr) + t0 <= 0) {
                gameOver = true;
                win = false;
            }
        }
        if (checkForWin()) {
            gameOver = true;
            win = true;
        }
    }

    ofstream resFile("result.out", ios::binary);
    string res;
    if (win) {
        res = "WIN";
    } else {
        res = "LOSE";
    }
    resFile.write(reinterpret_cast<const char *>(&res), sizeof(res));
    cout << res << endl;
}

bool Maze::isSharpLine(string line) {
    if (line.size() != SHARPLINELENGTH) {
        return false;
    }
    for (int i = 0; i < line.size(); i++) {
        if (line[i] != '#') {
            return false;
        }
    }
    return true;
}

void Maze::inputFromFiles() {
    ifstream mazetxt("maze.txt", ios::in);
    bool lastLine = false;
    string line;
    int n = 0; //number of rows
    int m = -1; //number of columns
    while (!lastLine) {
        getline(mazetxt, line);
        map.push_back(line);
        lastLine = findS(line);
        n++;
        m = line.size();
    }
    mazetxt.close();

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            string address = "Doors\\";
            address += convertIntToStr(i);
            address += "_";
            address += convertIntToStr(j);
            address += ".txt";
            ifstream door(address, ios::in);
            if (door) { //There is a door!
                Doors.push_back(Door(i, j));

                string type;
                getline(door, type);
                Doors.back().setIsBad(type == "BAD");
                if (Doors.back().getIsBad()) { //BADZAT
                    getline(door, line); //ignore ! characters
                    getline(door, line);
                    while (!isSeparatorLine(line)) {
                        Doors.back().pushToProblem(line);
                        getline(door, line);
                    }

                    getline(door, line);
                    while (!isSeparatorLine(line)) {
                        Doors.back().pushToSolution(line);
                        getline(door, line);
                    }

                    door >> line;
                    int t;
                    if (isSharpLine(line)) {
                        t = DEFAULT_DEC;
                    } else {
                        t = convertStrToInt(line);
                    }
                    Doors.back().setT(t);
                }

                else { //KHOSHZAT
                    door >> line;
                    int t;
                    if (isSharpLine(line)) {
                        t = DEFAULT_T;
                    } else {
                        t = convertStrToInt(line);
                    }
                    Doors.back().setT(t);
                }
            }
        }
    }
}

#endif
