#ifndef DOOR_H
#define DOOR_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm> //std::find
#include <fstream>
#include <ctime>

using namespace std;

class Door {
    private:
    int row;
    int col;
    bool isBad;
    int t; //Decreament or increament value
    vector <string> problem; //In case of BADZAT
    vector <string> solution; //In case of BADZAT

    public:
    Door(int row, int col);
    Door(){}
    bool getIsBad() { return isBad; }
    void setIsBad(bool b) { isBad = b; }
    int getT() { return t; }
    void setT(int t) { this -> t = t; }
    void pushToProblem(string line) {problem.push_back(line);}
    void printProblem();
    void pushToSolution(string line) { solution.push_back(line); }
    bool checkSolution(string givenSolution);
    int getRow() { return row; }
    int getCol() { return col; }
};

bool Door::checkSolution(string givenSolution) {
    return givenSolution == solution[0];
}

void Door::printProblem() {
    for (int i = 0; i < problem.size(); i++) {
        cout << problem[i] << endl;
    }
}

Door::Door(int row, int col) {
    this -> row = row;
    this -> col = col;
}

#endif // DOOR_H
