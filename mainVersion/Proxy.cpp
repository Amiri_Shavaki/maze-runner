#include "Maze.h"
#include "Proxy.h"

Proxy::Proxy() : ptr (new Maze){}

void Proxy::inputFromFiles() {
    ptr -> inputFromFiles();
}

void Proxy::setPlayerInitPos() {
    ptr -> setPlayerInitPos();
}

void Proxy::mainLoop() {
    ptr -> mainLoop();
}

Proxy::~Proxy() {
    delete ptr;
}
