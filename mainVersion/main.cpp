#include "Proxy.h"

int main() {
    Proxy maze;
    maze.inputFromFiles();
    maze.setPlayerInitPos();
    maze.mainLoop();
}
