#include <iostream>
#include <string>
#include <vector>
#include <algorithm> //std::find

using namespace std;

const int SEPARATORLINELENGTH = 10;
const int SHARPLINELENGTH = 10;

bool findS(string s) {
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == 'S') {
            return true;
        }
    }
    return false;
}

bool isSeparatorLine(string line) {
    if (line.size() != SEPARATORLINELENGTH) {
        return false;
    }
    for (int i = 0; i < line.size(); i++) {
        if (line[i] != '!') {
            return false;
        }
    }
    return true;
}

bool isSharpLine(string line) {
    if (line.size() != SHARPLINELENGTH) {
        return false;
    }
    for (int i = 0; i < line.size(); i++) {
        if (line[i] != '#') {
            return false;
        }
    }
    return true;
}

int convertStrToInt(string s) {
    int ans = 0;
    for (int i = 0; i < s.size(); i++) {
        ans += (s[i] - '0');
        ans *= 10;
    }
    return ans;
}

class Door {
    private:
    int row;
    int col;
    bool isBad;
    int t; //Decreament or increament value
    vector <string> problem; //In case of BADZAT
    vector <string> solution; //In case of BADZAT

    public:
    Door(int row, int col);
    Door(){}
    bool getIsBad() { return isBad; }
    void setIsBad(bool b) { isBad = b; }
    int getT() { return t; }
    void setT(int t) { this -> t = t; }
    void pushToProblem(string line) { problem.push_back(line); }
    void printProblem();
    void pushToSolution(string line) { solution.push_back(line); }
    bool checkSolution(string givenSolution);
    int getRow() { return row; }
    int getCol() { return col; }
};

bool Door::checkSolution(string givenSolution) {
    return givenSolution == solution[0];
}

void Door::printProblem() {
    for (int i = 0; i < problem.size(); i++) {
        cout << problem[i] << endl;
    }
}

Door::Door(int row, int col) {
    this -> row = row;
    this -> col = col;
}

class Player {
    friend class Maze;

    private:
    int row;
    int col;

    public:
    int getRow() { return row; }
    int getCol() { return col; }
    void move(char dir);
    void moveBack(char dir);
};

void Player::moveBack(char dir) {
    const int yDir[] = {1, 0, -1, 0};
    const int xDir[] = {0, 2, 0, -2};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    for (int i = 0; i < 4; i++) {
        if (cDir[i] == dir) {
            row += yDir[i];
            col += xDir[i];
        }
    }    
}

void Player::move(char dir) {
    const int yDir[] = {-1, 0, 1, 0};
    const int xDir[] = {0, -2, 0, 2};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    for (int i = 0; i < 4; i++) {
        if (cDir[i] == dir) {
            row += yDir[i];
            col += xDir[i];
        }
    }
}

class Maze {
    private:
    vector <string> map;
    vector <Door> Doors;
    const int DEFAULT_DEC;
    const int DEFAULT_T;
    Player player;

    public:
    void inputFromFiles();
    Maze(): DEFAULT_DEC(4321), DEFAULT_T(12345) {}
    void mainLoop();
    void setPlayerInitPos();
    void printMap();
    vector <char> printOptions();
    bool doorProcess();
    void clearMap();
    bool checkForWin();
};

bool Maze::checkForWin() {
    if (player.row > 1) {
        return false;
    }
    for (int i = 0; i < map[0].size(); i++) {
        if (map[0][i] == 'O' && player.col - 1 == i) {
            return true;
        }
    }
    return false;
}

void Maze::clearMap() {
    char playerPosValue = map[player.row - 1][player.col - 1];
    if (playerPosValue == '#' || playerPosValue == 'T') {
        map[player.row - 1][player.col - 1] = ' ';
    }
}

bool Maze::doorProcess() {
    bool ans = true;
    for (int i = 0; i < Doors.size(); i++) {
        if (Doors[i].getRow() == player.row && Doors[i].getCol() == player.col && map[player.row - 1][player.col - 1] != ' ') {
            if (Doors[i].getIsBad()) { //BADZAT
                Doors[i].printProblem();
                string givenSolution;
                cin.ignore();
                getline(cin, givenSolution);
                ans = Doors[i].checkSolution(givenSolution);
            } /*else { //KHOSHZAT
            }*/
        }
    }
    return ans;
}

vector <char> Maze::printOptions() {
    vector <char> ans;

    int y = player.row - 1; //0-based
    int x = player.col - 1; //0-based
    int n = map.size(); //number of rows
    int m = map[0].size(); //number of columns

    //correct order: U L D R
    const int yDir[] = {-1, 0, 0, 0};
    const int xDir[] = {0, -1, 0, 1};
    const char cDir[] = {'U', 'L', 'D', 'R'};
    const string destDir[] = {"up", "left", "down", "right"};
    bool firstTime = true; //just a simple flag
    for (int i = 0; i < 4; i++) {
        int newY = y + yDir[i];
        int newX = x + xDir[i];
        if (newY >= n || newY < 0 || newX >= m || newX < 0) { //Can't go in the direction
            continue;
        }
        if (map[newY][newX] == '|' || map[newY][newX] == '_' || map[newY][newX] == 'S') { //Can't go in the direction
            continue;
        }
        if (firstTime) {
            firstTime = false;
        } else {
            cout << ", ";
        }
        cout << "Enter " << cDir[i] << " to go " << destDir[i];

        ans.push_back(cDir[i]);
    }
    cout << endl;
    return ans;
}

void Maze::printMap() {
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map[i].size(); j++) {
            if (player.row == i + 1 && player.col == j + 1) { //1-based conversion
                cout << 'P';
            } else if (map[i][j] == 'S' || map[i][j] == 'O') {
                cout << ' ';
            } else {
                cout << map[i][j];
            }
        }
        cout << endl;
    }
}

void Maze::setPlayerInitPos() {
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map[i].size(); j++) {
            if (map[i][j] == 'S') {
                player.row = i + 1; //1-based
                player.col = j + 1; //1-based
            }
        }
    }
}

void Maze::mainLoop() {
    bool gameOver = false;
    string choice;
    bool win;
    while (!gameOver) {
        printMap();
        vector <char> options = printOptions();
        cin >> choice;
        if (find(options.begin(), options.end(), choice[0]) != options.end()) {
            player.move(choice[0]);
            if (!doorProcess()) {
                player.moveBack(choice[0]);
            } else {
                clearMap();
            }
        }
        if (checkForWin()) {
            gameOver = true;
            win = true;
        }
    }
    if (win) {
        cout << "WIN";
    } /*else {

    }*/
    cout << endl;
}

void Maze::inputFromFiles() {
    //maze.txt
    bool lastLine = false;
    string line;
    while (!lastLine) {
        getline(cin, line);
        map.push_back(line);
        lastLine = findS(line);
    }

    //test given input
    //printVectorString(map);

    int numOfDoors;
    cin >> numOfDoors;

    for (int i = 0; i < numOfDoors; i++) {
        int row, col;
        cin >> row >> col;
        Doors.push_back(Door(row, col));

        string type;
        cin >> type;
        Doors[i].setIsBad(type == "BAD");

        if (Doors[i].getIsBad()) { //BADZAT
            cin >> line; //ignore ! characters

            getline(cin, line);
            while (!isSeparatorLine(line)) {
                Doors[i].pushToProblem(line);
                getline(cin, line);
            }

            getline(cin, line);
            while (!isSeparatorLine(line)) {
                Doors[i].pushToSolution(line);
                getline(cin, line);
            }

            cin >> line;
            int t;
            if (isSharpLine(line)) {
                t = DEFAULT_DEC;
            } else {
                t = convertStrToInt(line);
            }
            Doors[i].setT(t);
        }

        else { //KHOSHZAT
            cin >> line;
            int t;
            if (isSharpLine(line)) {
                t = DEFAULT_T;
            } else {
                t = convertStrToInt(line);
            }
            Doors[i].setT(t);
        }
    }
}

int main() {
    Maze maze;
    maze.inputFromFiles();
    maze.setPlayerInitPos();
    maze.mainLoop();
}